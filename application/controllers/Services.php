<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
	}

	public function page($id)
	{
		$data = $this->main->data_front();
		$data['page'] = $this->db->where('id', $id)->get('category')->row();
		$data['page']->type = 'services';
		$data['tour'] = $this->db->where('id_category', $id)->get('tour')->result();

		$this->template->front('services', $data);
	}

	public function detail($id)
	{
		$data = $this->main->data_front();
		$data['page'] = $this->db->where('id', $id)->get('tour')->row();
		$data['page']->type = 'services';
		$data['gallery'] = $this->db->where('id_tour', $id)->get('tour_gallery')->result();
		$data['related'] = $this->db
			->where('id_category', $data['page']->id_category)
			->where_not_in('id', array($id))
			->get('tour')
			->result();
		$this->template->front('services_detail', $data);
	}
}
