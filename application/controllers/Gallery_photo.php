<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_photo extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
	}

	public function index()
	{
		$data = $this->main->data_front();
		$data['page'] = $this->db->where('type', 'gallery_photo')->get('pages')->row();
		$data['gallery'] = $this->db->where('use', 'yes')->order_by('id', 'ASC')->get('gallery')->result();
		$this->template->front('gallery_photo', $data);
	}
}
