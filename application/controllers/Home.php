<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
	}

	public function index()
	{
		$data = $this->main->data_front();
		$data['page'] = $this->db->where('type', 'home')->get('pages')->row();
		$data['slider'] = $this->db->where('use', 'yes')->get('slider')->result();
		$data['tour'] = $this->db->where('use', 'yes')->order_by('title', 'ASC')->get('tour', 9, 0)->result();
		$this->template->front_slider('home', $data);

	}
}
