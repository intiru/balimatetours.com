<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_pages');
		$this->load->library('main');
		$this->main->check_admin();
	}

	public function type($type)
	{
		$data = $this->main->data_main();
		$where = array(
			'type' => $type
		);

		$data['row'] = $this->m_pages->row_data($where);
		$this->template->set('about', 'kt-menu__item--active');
		$this->template->set('breadcrumb', 'Management ' . $data['row']->title);
		$this->template->load_admin('pages/index', $data);
	}

	public function update($id)
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('meta_title', 'Meta title', 'required');
		$this->form_validation->set_rules('meta_description', 'Meta Description', 'required');
		$this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'required');
		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'title' => form_error('title'),
					'description' => form_error('description'),
					'meta_title' => form_error('meta_title'),
					'meta_description' => form_error('meta_description'),
					'meta_keywords' => form_error('meta_keywords'),
				)
			));
		} else {

			$data = $this->input->post(NULL, TRUE);
			$where = array(
				'id' => $id
			);

			$this->m_pages->update_data($where, $data, 'about');
			echo json_encode(array(
				'status' => 'success',
				'message' => 'data berhasil diinput'
			));
		}

	}
}
