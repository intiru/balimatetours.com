<div class="sec-padding section-dark">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-xs-12 clearfix margin-bottom">
				<div class="footer-logo"><img src="<?php echo base_url() ?>assets/front/images/logo/logo.png" alt=""/>
				</div>
				<div class="clearfix"></div>
				<address class="text-light">

					2877 Short Street, New York, NY 7510
					ipsum City, Country
				</address>
				<span class="text-light"> 406-822-6469</span><br>
				<span class="text-light"> support@domain.com </span><br>
				<span class="text-light"> 406-382-9305</span>
				<ul class="footer-social-icons white left-align icons-plain text-center">
					<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a class="active" href="#"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
				</ul>
			</div>
			<div class="col-md-7 col-xs-12 clearfix margin-bottom">
				<h4 class="text-white less-mar3 font-weight-5">Menu</h4>
				<div class="clearfix"></div>
				<br/>
				<ul class="footer-quick-links-4 white">
					<li><a href="#"><i class="fa fa-angle-right"></i> Home</a></li>
					<li><a href="#"><i class="fa fa-angle-right"></i> Services</a></li>
					<li><a href="#"><i class="fa fa-angle-right"></i> Testimonial</a></li>
					<li><a href="#"><i class="fa fa-angle-right"></i> Gallery Photo</a></li>
					<li><a href="#"><i class="fa fa-angle-right"></i> About Us</a></li>
					<li><a href="#"><i class="fa fa-angle-right"></i> Contact Us</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<section class="sec-padding-6 section-medium-dark">
	<div class="container">
		<div class="row">
			<div class="fo-copyright-holder text-center"> Copyright © 2019 l yourdomain.com. All rights reserved. </div>
		</div>
	</div>
</section>
<div class="clearfix"></div>


<a href="#" class="scrollup"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
