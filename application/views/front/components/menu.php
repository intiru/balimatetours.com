<div class="mod-menu">
	<div class="row">
		<div class="col-sm-2">
			<a href="<?php echo base_url() ?>" title="" class="logo style-2 mar-4">
				<img src="<?php echo base_url() ?>assets/front/images/logo/logo.png" alt="">
			</a>
		</div>
		<div class="col-sm-10">
			<div class="main-nav">
				<?php if ($view_secret) { ?>
					<ul class="nav navbar-nav top-nav">
						<li class="search-parent"><a href="javascript:void(0)" title=""><i aria-hidden="true"
																						   class="fa fa-search"></i></a>
							<div class="search-box ">
								<div class="content">
									<div class="form-control">
										<input type="text" placeholder="Type to search"/>
										<a href="#" class="search-btn mar-1"><i aria-hidden="true"
																				class="fa fa-search"></i></a></div>
									<a href="#" class="close-btn mar-1">x</a></div>
							</div>
						</li>
						<li class="visible-xs menu-icon">
							<a href="javascript:void(0)" class="navbar-toggle collapsed" data-toggle="collapse" data-
							   target="#menu" aria-expanded="false">
								<i aria-hidden="true" class="fa fa-bars"></i>
							</a>
						</li>
					</ul>
				<?php } ?>
				<div id="menu" class="collapse">
					<ul class="nav navbar-nav">
						<li class="right <?php echo $page->type == 'home' ? 'active' : '' ?>"><a
								href="<?php echo base_url() ?>">Home</a></li>
						<li class="right <?php echo $page->type == 'services' ? 'active' : '' ?>">
							<a href="#">Services</a>
							<span class="arrow"></span>
							<ul class="dm-align-2">
								<?php foreach ($services_list as $r) { ?>
									<li><a href="<?php echo $this->main->permalink(array($r->title)) ?>"><?php echo $r->title ?></a></li>
								<?php } ?>
							</ul>
						</li>
						<li class="right <?php echo $page->type == 'testimonial' ? 'active' : '' ?>"><a
								href="<?php echo base_url('testimonial') ?>">Testimonial</a></li>
						<li class="right <?php echo $page->type == 'gallery_photo' ? 'active' : '' ?>"><a
								href="<?php echo base_url('gallery-photo') ?>">Gallery Photo</a></li>
						<li class="right <?php echo $page->type == 'about_us' ? 'active' : '' ?>"><a
								href="<?php echo base_url('about-us') ?>">About Us</a></li>
						<li class="right <?php echo $page->type == 'contact_us' ? 'active' : '' ?>"><a
								href="<?php echo base_url('contact-us') ?>">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

</div>
