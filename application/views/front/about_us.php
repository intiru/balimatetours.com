<section>
	<div class="pagenation-holder">
		<div class="container">
			<div class="row">
				<div class="col-md-6"><h1 class="uppercase"><?php echo $page->title ?></h1></div>
				<div class="col-md-6">
					<ol class="breadcrumb">
						<li class="current"><a href="<?php echo base_url() ?>">Home</a></li>
						<li><?php echo $page->title ?></li>
					</ol>
				</div>

			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>



<section class="sec-padding">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-centered">
				<div class="sec-title-container">
					<div class="ce-title-line"></div>
					<div class="clearfix"></div>
					<div class="col-md-12 nopadding">
						<?php echo $page->description ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




