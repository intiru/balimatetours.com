<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $page->meta_title ?></title>
	<meta name="keywords" content="<?php echo $page->meta_keywords ?>" />
	<meta name="description" content="<?php echo $page->meta_description ?>">
	<meta name="author" content="<?php echo $author ?>">
	<meta name="revisit-after" content="2 days" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="General" />
	<meta http-equiv="charset" content="ISO-8859-1" />
	<meta http-equiv="content-language" content="English" />
	<meta name="MSSmartTagsPreventParsing" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.png">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/js/bootstrap/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Yesteryear" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/js/megamenu/stylesheets/screen.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/theme-default.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/corporate.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/fonts/font-awesome/css/font-awesome.min.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/fonts/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />
	<link rel="stylesheet" href="f<?php echo base_url() ?>assets/front/onts/et-line-font/et-line-font.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/js/revolution-slider/css/settings.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/js/revolution-slider/css/layers.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/js/revolution-slider/css/navigation.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/js/parallax/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/js/cubeportfolio/cubeportfolio.min.css">
	<link href="<?php echo base_url() ?>assets/front/js/owl-carousel/owl.carousel.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/front/js/owl-carousel/owl.theme.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/front/js/tabs/css/responsive-tabs.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/js/ytplayer/ytplayer.css" />
	<link href="<?php echo base_url() ?>assets/front/js/accordion/css/smk-accordion.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/shortcodes.css" type="text/css">

	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/custom.css">
</head>

<body>

<div class="wrapper-boxed">
	<div class="site-wrapper">
		<div class="col-md-12 nopadding">
			<div class="header-section dark-dropdowns style1 pin-style">
				<div class="container">
					<?php echo $menu ?>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="slide-tmargin">
			<div class="slidermaxwidth">
				<div class="rev_slider_wrapper">
					<div id="rev_slider" class="rev_slider"  data-version="5.0">
						<ul>
							<?php foreach($slider as $r) { ?>
							<li data-index="rs-1" data-transition="fade">
								<img src="<?php echo base_url('upload/images/'.$r->thumbnail) ?>" alt="<?php echo $r->thumbnail_alt ?>" title="<?php echo $r->title ?>">
								<div class="tp-caption montserrat fweight-6 text-white tp-resizeme"
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
									 data-y="['top','top','top','top']" data-voffset="['320','100','100','110']"
									 data-fontsize="['70','70','50','30']"
									 data-lineheight="['100','100','100','50']"
									 data-width="none"
									 data-height="none"
									 data-whitespace="nowrap"
									 data-transform_idle="o:1;"
									 data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
									 data-start="1000"
									 data-splitin="none"
									 data-splitout="none"
									 data-responsive_offset="on"
									 style="z-index: 7; white-space: nowrap;">
									<?php echo $r->title ?>
								</div>
								<div class="tp-caption montserrat fweight-1 text-white tp-resizeme font-size-6"
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
									 data-y="['top','top','top','top']" data-voffset="['380','150','150','150']"
									 data-fontsize="['70','60','50','30']"
									 data-lineheight="['100','100','100','50']"
									 data-width="none"
									 data-height="none"
									 data-whitespace="nowrap"
									 data-transform_idle="o:1;"
									 data-transform_in="x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
									 data-start="1500"
									 data-splitin="none"
									 data-splitout="none"
									 data-responsive_offset="on"
									 style="z-index: 7; white-space: nowrap;">
									<?php echo $r->description ?>
								</div>
								<div class="tp-caption sbut2"
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
									 data-y="['top','top','top','top']" data-voffset="['550','350','370','300']"
									 data-speed="800"
									 data-start="2000"
									 data-transform_in="y:bottom;s:1500;e:Power3.easeOut;"
									 data-transform_out="opacity:0;s:3000;e:Power4.easeIn;s:3000;e:Power4.easeIn;"
									 data-endspeed="300"
									 data-captionhidden="off"
									 style="z-index: 6">
									<a href="<?php echo $r->url ?>" class="btn btn-large btn-orange btn-anim-4 uppercase xround-7">
										Get Started Now!
									</a>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>

		<?php echo $content ?>

		<?php echo $footer ?>

	</div>
</div>
<span class="base-value" data-base-url="<?php echo base_url() ?>"></span>

<script src="<?php echo base_url() ?>assets/front/js/jquery/jquery.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/less/less.min.js" data-env="development"></script>

<script src="<?php echo base_url() ?>assets/front/js/megamenu/js/main.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/ytplayer/jquery.mb.YTPlayer.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/ytplayer/elementvideo-custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/ytplayer/play-pause-btn.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript">
    var tpj=jQuery;
    var revapi4;
    tpj(document).ready(function() {
        if(tpj("#rev_slider").revolution == undefined){
            revslider_showDoubleJqueryError("#rev_slider");
        }else{
            revapi4 = tpj("#rev_slider").show().revolution({
                sliderType:"standard",
                jsFileLocation:"js/revolution-slider/js/",
                sliderLayout:"auto",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                    keyboardNavigation:"off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation:"off",
                    onHoverStop:"off",
                    arrows: {
                        style:"uranus",
                        enable:true,
                        hide_onmobile:false,
                        hide_under:100,
                        hide_onleave:true,
                        hide_delay:200,
                        hide_delay_mobile:1200,
                        tmp:'',
                        left: {
                            h_align:"left",
                            v_align:"center",
                            h_offset:80,
                            v_offset:0
                        },
                        right: {
                            h_align:"right",
                            v_align:"center",
                            h_offset:80,
                            v_offset:0
                        }
                    }
                    ,
                    touch:{
                        touchenabled:"on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                    ,



                },
                viewPort: {
                    enable:true,
                    outof:"pause",
                    visible_area:"80%"
                },

                responsiveLevels:[1240,1024,778,480],
                gridwidth:[1240,1024,778,480],
                gridheight:[880,730,600,550],
                lazyType:"smart",
                parallax: {
                    type:"mouse",
                    origo:"slidercenter",
                    speed:2000,
                    levels:[2,3,4,5,6,7,12,16,10,50],
                },
                shadow:0,
                spinner:"off",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                disableProgressBar:"on",
                debugMode:false,
                fallbacks: {
                    simplifyAll:"off",
                    nextSlideOnWindowFocus:"off",
                    disableFocusListener:false,
                }
            });
        }
    }); /*ready*/
</script>

<script src="<?php echo base_url() ?>assets/front/js/loaders/loading-custom.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/parallax/parallax-background.min.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/parallax/parallax-custom.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/tabs/js/responsive-tabs.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/cubeportfolio/main-mosaic3-cols3.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/owl-carousel/custom.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/accordion/js/smk-accordion.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/accordion/js/custom.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/progress-circle/raphael-min.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/progress-circle/custom.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/progress-circle/jQuery.circleProgressBar.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/functions/functions.js"></script>
</body>
</html>
