
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $page->meta_title ?></title>
	<meta name="keywords" content="<?php echo $page->meta_keywords ?>" />
	<meta name="description" content="<?php echo $page->meta_description ?>">
	<meta name="author" content="<?php echo $author ?>">
	<meta name="revisit-after" content="2 days" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="General" />
	<meta http-equiv="charset" content="ISO-8859-1" />
	<meta http-equiv="content-language" content="English" />
	<meta name="MSSmartTagsPreventParsing" content="true" />
	<!-- Mobile view -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.png">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/js/bootstrap/bootstrap.min.css">

	<!-- Google fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Yesteryear" rel="stylesheet">

	<!-- Template's stylesheets -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/js/megamenu/stylesheets/screen.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/theme-default.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/js/loaders/stylesheets/screen.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/corporate.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/fonts/font-awesome/css/font-awesome.min.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/fonts/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/fonts/et-line-font/et-line-font.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/js/revolution-slider/css/settings.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/js/revolution-slider/css/layers.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/js/revolution-slider/css/navigation.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/js/parallax/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/front/js/cubeportfolio/cubeportfolio.min.css">
	<link href="<?php echo base_url() ?>assets/front/js/owl-carousel/owl.carousel.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/front/js/owl-carousel/owl.theme.css" rel="stylesheet">
	<link href="<?php echo base_url() ?>assets/front/js/tabs/css/responsive-tabs.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/js/jFlickrFeed/style.css" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/custom.css">



	<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/css/shortcodes.css" type="text/css">

	<script src="<?php echo base_url() ?>assets/front/js/jquery/jquery.js"></script>
	<script src="<?php echo base_url() ?>assets/front/js/bootstrap/bootstrap.min.js"></script>


	<!-- Template's stylesheets END -->

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Style Customizer's stylesheets -->

	<link rel="stylesheet/less" type="text/css" href="<?php echo base_url() ?>assets/front/less/skin.less">
	<!-- Style Customizer's stylesheets END -->

	<!-- Skin stylesheet -->

</head>

<body>
<!--end loading-->



<div class="wrapper-boxed">
	<div class="site-wrapper">
		<div class="col-md-12 nopadding">
			<div class="header-section dark dark-dropdowns style1 links-dark pin-style">
				<div class="container">
					<?php echo $menu ?>
				</div>
			</div>
			<!--end menu-->

		</div>
		<div class="clearfix"></div>


		<?php echo $content ?>

		<?php echo $footer ?>

	</div>
</div>
<span class="base-value" data-base-url="<?php echo base_url() ?>"></span>
<!--end wrapper boxed-->

<!-- Scripts -->
<script src="<?php echo base_url() ?>assets/front/js/less/less.min.js" data-env="development"></script>
<!-- Scripts END -->

<!-- Template scripts -->
<script src="<?php echo base_url() ?>assets/front/js/megamenu/js/main.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/ytplayer/jquery.mb.YTPlayer.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/ytplayer/elementvideo-custom.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/ytplayer/play-pause-btn.js"></script>
<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

</body>
</html>
