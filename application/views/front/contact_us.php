<section>
	<div class="pagenation-holder">
		<div class="container">
			<div class="row">
				<div class="col-md-6"><h1 class="uppercase"><?php echo $page->title ?></h1></div>
				<div class="col-md-6">
					<ol class="breadcrumb">
						<li class="current"><a href="<?php echo base_url() ?>">Home</a></li>
						<li><?php echo $page->title ?></li>
					</ol>
				</div>

			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>

<section class="sec-padding">
	<div class="container">
		<div class="row">

			<div class="col-md-8">

				<div class="one_half">


					<div class="cforms_sty3">


						<div id="form_status"></div>
						<form type="POST" id="gsr-contact" onsubmit="return valid_datas( this );">
							<label class="label">Name <em>*</em></label>
							<label class="input">
								<input type="text" name="name" id="name">
							</label>

							<div class="clearfix"></div>

							<label class="label">E-mail <em>*</em></label>
							<label class="input">
								<input type="email" name="email" id="email">
							</label>
							<div class="clearfix"></div>

							<label class="label">Subject <em>*</em></label>
							<label class="input">
								<input type="text" name="subject" id="subject">
							</label>

							<div class="clearfix"></div>

							<label class="label">Message <em>*</em></label>
							<label class="textarea">
								<textarea rows="5" name="message" id="message"></textarea>
							</label>

							<div class="clearfix"></div>
							<input type="hidden" name="token" value="FsWga4&amp;@f6aw">
							<button type="submit" class="button">Send Message</button>

						</form>


					</div>

				</div>

			</div>
			<!--end item-->

			<div class="col-md-4 text-left">
				<h3><strong>Get in Touch</strong></h3>
				<br>
				<h4>Address Info</h4>
				<h6>Company name</h6>
				<p>3096 Cemetery Hollow Street, Houston, TX 77099
					Telephone: +1 1234-567-89000
					FAX: +1 0123-4567-8900</p>
				<br>
				<p>E-mail: mail@companyname.com</p>
				<p>Website: www.yoursitename.com</p>
			</div>
			<!--end item-->



		</div>
	</div>
</section>

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15780.721936937447!2d115.22448505!3d-8.578638649999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd23e98ce81d05d%3A0xeba44397531d427f!2sGreen%20Village%20Bali!5e0!3m2!1sid!2sid!4v1570255584559!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>



