<section class="sec-padding">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-centered">
				<div class="sec-title-container">
					<div class="ce-title-line"></div>
					<div class="clearfix"></div>
					<h2 class="font-weight-12 less-mar-1 line-height-5"><?php echo $page->title ?></h2>
					<div class="col-md-12 nopadding">
						<p class="font-weight-4 line-height-3 text-justify">
							<?php echo $page->description ?>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row tour-wrapper">
			<?php foreach ($tour as $r) { ?>
				<a href="<?php echo $this->main->permalink(array($r->title)) ?>" class="col-md-4">
					<div class="ce-feature-box-3">
						<div class="img-box">
							<div class="text-box">
								<h5 class="title font-weight-5 title"><?php echo $r->title ?></h5>
								<p class="content text-justify"><?php echo $this->main->short_desc($r->description) ?></p>
								<div class="text-center">
									<button class="btn btn-small btn-orange btn-anim-1 uppercase xround-4">Detail
									</button>
								</div>
							</div>
							<img src="<?php echo $this->main->image_preview_url($r->thumbnail) ?>"
								 alt="<?php echo $r->thumbnail_alt ?>" class="img-responsive" width="100%"/>
						</div>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>
</section>
