<?php

Class Main
{

	private $ci;
	private $web_name = 'Bali Mate Tours';
	private $file_info = 'Images with resolution 800px x 600px and size 100KB';
	private $file_info_slider = 'Images with resolution 1920px x 900px and size 250KB';
	private $path_images = 'upload/images/';
	private $image_size_preview = 200;
	private $help_thumbnail_alt = 'Penting untuk SEO Gambar';
	private $help_meta = 'Penting untuk SEO Halaman Website';
	private $short_desc_char = 100;

	public function __construct()
	{
		$this->ci =& get_instance();
	}

	function short_desc($string) {
		return substr(strip_tags($string), 0, $this->short_desc_char).' ...';
	}

	function web_name() {
		return $this->web_name;
	}

	function date_view($date) {
		return date('d F Y', strtotime($date));
	}

	function help_thumbnail_alt()
	{
		return $this->help_thumbnail_alt;
	}

	function help_meta()
	{
		return $this->help_meta;
	}

	function file_info()
	{
		return $this->file_info;
	}

	function file_info_slider()
	{
		return $this->file_info_slider;
	}

	function path_images()
	{
		return $this->path_images;
	}

	function image_size_preview()
	{
		return $this->image_size_preview;
	}

	function image_preview_url($filename)
	{
		return base_url($this->path_images . $filename);
	}

	function delete_file($filename)
	{
		if ($filename) {
			if (file_exists(FCPATH . $this->path_images . $filename)) {
				unlink($this->path_images . $filename);
			}
		}
	}

	function data_main()
	{
		$data = array(
			'web_name' => $this->web_name,
			'menu_list' => $this->menu_list(),
			'name' => $this->ci->session->userdata('name'),
		);

		return $data;
	}

	function data_front()
	{
		$data = array(

			'alamat' => 'asdf',
			'telephone' => '12',
			'phone' => '12',
			'whatsapp' => '123',
			'whatsapp_link' => '123',
			'wechat_id' => '123',
			'wechat_link' => '123',
			'email_link' => '123',
			'email' => '123',
			'facebook' => '',
			'twitter' => '',
			'linkedin' => '',
			'instagram' => '',
			'view_secret' => FALSE,
			'author' => 'www.balimateservices.com',
			'services_list' => $this->ci->db->select('title')->order_by('title', 'ASC')->get('category')->result()
		);

		return $data;
	}

	function check_admin()
	{
		if ($this->ci->session->userdata('status') !== 'login') {
			redirect('login');
		}
	}

	function check_login()
	{
		if ($this->ci->session->userdata('status') == 'login') {
			redirect('intiru/dashboard');
		}
	}

	function permalink($data)
	{
		$slug = '';
		foreach ($data as $r) {
			$slug .= strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', $r)).'/';
		}

		return base_url($slug);
	}

	function breadcrumb($data) {
		$breadcrumb = '<ul class="breadcrumb">';
		$count = count($data);
		$no = 1;
		foreach($data as $url => $label) {
			$current = '';
			if($no == $count) {
				$current = ' class="current"';
			}

			$breadcrumb .= '<li'.$current.'><a href="'.$url.'">'.$label.'</a></li>';
		}

		$breadcrumb .= '</ul>';


		return $breadcrumb;
	}

	function slug($text)
	{
		$text = trim($text);
		if (empty($text)) return '';
		$text = preg_replace("/[^a-zA-Z0-9\-\s]+/", "", $text);
		$text = strtolower(trim($text));
		$text = str_replace(' ', '-', $text);
		$text = $text_ori = preg_replace('/\-{2,}/', '-', $text);
		return $text;
	}

	function slug_back($slug)
	{
		$slug = trim($slug);
		if (empty($slug)) return '';
		$slug = str_replace('-', ' ', $slug);
		$slug = ucwords($slug);
		return $slug;
	}

	function upload_file_thumbnail($fieldname, $filename)
	{
		$config['upload_path'] = './upload/images/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = 100;
		$config['max_width'] = 800;
		$config['max_height'] = 600;
		$config['overwrite'] = TRUE;
		$config['file_name'] = $this->slug($filename);
		$this->ci->load->library('upload', $config);

		if (!$this->ci->upload->do_upload($fieldname)) {
			return array(
				'status' => FALSE,
				'message' => $this->ci->upload->display_errors()
			);
		} else {
			return array(
				'status' => TRUE,
				'filename' => $data['thumbnail'] = $this->ci->upload->file_name
			);
		}
	}

	function upload_file_slider($fieldname, $filename)
	{
		$config['upload_path'] = './upload/images/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = 250;
		$config['max_width'] = 1920;
		$config['max_height'] = 900;
		$config['overwrite'] = TRUE;
		$config['file_name'] = $this->slug($filename);
		$this->ci->load->library('upload', $config);

		if (!$this->ci->upload->do_upload($fieldname)) {
			return array(
				'status' => FALSE,
				'message' => $this->ci->upload->display_errors()
			);
		} else {
			return array(
				'status' => TRUE,
				'filename' => $data['thumbnail'] = $this->ci->upload->file_name
			);
		}
	}

	function captcha() {
		$this->ci->load->helper(array('captcha','string'));
		$this->ci->load->library('session');

		$vals = array(
			'img_path' => './upload/images/',
			'img_url' => base_url() . 'upload/images/',
			'img_width' => '200',
			'img_height' => 35,
			'border' => 0,
			'expiration' => 7200,
			'word' => random_string('numeric', 5)
		);

		// create captcha image
		$cap = create_captcha($vals);

		// store image html code in a variable
		$captcha = $cap['image'];

		// store the captcha word in a session
		//$cap['word'];
		$this->ci->session->set_userdata('captcha_mwz', $cap['word']);

		return $captcha;
	}

	function mailer_auth($subject, $to_email, $to_name, $body) {
		$this->ci->load->library('my_phpmailer');
		$mail = new PHPMailer;

		try {
			$mail->IsSMTP();
			$mail->SMTPSecure = "ssl";
			$mail->Host = "smtp.gmail.com"; //hostname masing-masing provider email
			$mail->SMTPDebug = 2;
			$mail->SMTPDebug = false;
			$mail->do_debug = 0;
			$mail->Port = 465;
			$mail->SMTPAuth = true;
			$mail->Username = "baliboatticket@gmail.com"; //user email
			$mail->Password = "Flores2302"; //password email
			$mail->SetFrom("baliboatticket@gmail.com", "Bali Boat Ticket"); //set email pengirim
			$mail->Subject = $subject; //subyek email
			$mail->AddAddress($to_email, $to_name); //tujuan email
			$mail->MsgHTML($body);
			$mail->Send();
			//echo "Message has been sent";
		} catch (phpmailerException $e) {
			echo $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
			echo $e->getMessage(); //Boring error messages from anything else!
		}
	}

	function menu_list()
	{
		$menu = array(
			'MAIN' => array(
				'dashboard' => array(
					'label' => 'Dashboard',
					'route' => base_url('intiru/dashboard'),
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array()
				),
				'view_front' => array(
					'label' => 'View Website',
					'route' => base_url(''),
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array()
				)
			),
			'PAGES' => array(
				'home' => array(
					'label' => 'Home',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'home_page' => array(
							'label' => 'Home Page',
							'route' => base_url('intiru/pages/type/home'),
							'icon' => 'fab fa-asymmetrik'
						),
						'home_slider' => array(
							'label' => 'Home Slider',
							'route' => base_url('intiru/home_slider'),
							'icon' => 'fab fa-asymmetrik'
						),
					)
				),
				'services' => array(
					'label' => 'Services',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
//						'services_page' => array(
//							'label' => 'Services Page',
//							'route' => base_url('intiru/pages/type/services'),
//							'icon' => 'fab fa-asymmetrik'
//						),
						'categories' => array(
							'label' => 'Sub Menu',
							'route' => base_url('intiru/category'),
							'icon' => 'fab fa-asymmetrik'
						),
						'services' => array(
							'label' => 'Services List',
							'route' => base_url('intiru/tour'),
							'icon' => 'fab fa-asymmetrik'
						),
						'services_gallery' => array(
							'label' => 'Services Gallery List',
							'route' => base_url('intiru/tour_gallery'),
							'icon' => 'fab fa-asymmetrik'
						),
						'reservation' => array(
							'label' => 'Reservation List',
							'route' => base_url('intiru/reservation'),
							'icon' => 'fab fa-asymmetrik'
						),
					)
				),
				'testimonial' => array(
					'label' => 'Testimonial',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'testimonial_page' => array(
							'label' => 'Testimonial Page',
							'route' => base_url('intiru/pages/type/testimonial'),
							'icon' => 'fab fa-asymmetrik'
						),
						'testimonial_list' => array(
							'label' => 'Testimonial List',
							'route' => base_url('intiru/testimonial'),
							'icon' => 'fab fa-asymmetrik'
						),
					)
				),
				'gallery_photo' => array(
					'label' => 'Gallery Photo',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'gallery_photo_page' => array(
							'label' => 'Gallery Photo Page',
							'route' => base_url('intiru/pages/type/gallery_photo'),
							'icon' => 'fab fa-asymmetrik'
						),
						'gallery_photo_list' => array(
							'label' => 'Gallery Photo List',
							'route' => base_url('intiru/gallery_photo'),
							'icon' => 'fab fa-asymmetrik'

						),
					)
				),
				'gallery_video' => array(
					'label' => 'Gallery Video',
					'route' => 'javascript:;',
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array(
						'gallery_photo_page' => array(
							'label' => 'Gallery Video Page',
							'route' => base_url('intiru/pages/type/gallery_video'),
							'icon' => 'fab fa-asymmetrik'
						),
						'gallery_photo_list' => array(
							'label' => 'Gallery Video List',
							'route' => base_url('intiru/gallery_video'),
							'icon' => 'fab fa-asymmetrik'

						),
					)
				),
				'about_us' => array(
					'label' => 'About Us',
					'route' => base_url('intiru/pages/type/about_us'),
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array()
				),
//				'blog' => array(
//					'label' => 'Blog',
//					'route' => 'javascript:;',
//					'icon' => 'fab fa-asymmetrik',
//					'sub_menu' => array(
//						'blog_page' => array(
//							'label' => 'Blog Page',
//							'route' => base_url('intiru/pages/type/blog'),
//							'icon' => 'fab fa-asymmetrik'
//						),
//						'blog_category' => array(
//							'label' => 'Blog Category',
//							'route' => base_url('intiru/blog_category'),
//							'icon' => 'fab fa-asymmetrik'
//						),
//						'blog_list' => array(
//							'label' => 'Blog Content',
//							'route' => base_url('intiru/blog_content'),
//							'icon' => 'fab fa-asymmetrik'
//						),
//					)
//				),
				'contact_us' => array(
					'label' => 'Contact Us',
					'route' => base_url('intiru/pages/type/contact_us'),
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array()
				),
				'reservation' => array(
					'label' => 'Reservation',
					'route' => base_url('intiru/pages/type/reservation'),
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array()
				),
			),
			'OTHERS MENU' => array(
				'email' => array(
					'label' => 'Email',
					'route' => base_url('intiru/email'),
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array()
				),
				'admin' => array(
					'label' => 'Manage Admin',
					'route' => base_url('intiru/admin'),
					'icon' => 'fab fa-asymmetrik',
					'sub_menu' => array()
				),
			),
		);

		return $menu;
	}
}
