$(document).ready(function () {

	var base_url = $('.base-value').attr('base-url');

	$('.form-send').submit(function (e) {
		e.preventDefault();

		$.ajax({
			url: $(this).attr('action'),
			type: $(this).attr('method'),
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData: false,
			success: function (json) {
				var data = JSON.parse(json);
				$('.help-block').remove();
				if (data.status === 'error') {
					swal.fire({
						position: 'center',
						type: 'warning',
						title: data.message,
					});

					$.each(data.errors, function (field, message) {
						if (message) {
							$('[name=' + field + ']').after('<div class="help-block error">' + message + '</div>');
						}
					})
				} else if (data.status === 'success') {
					swal.fire({
						position: 'center',
						type: 'success',
						title: data.message,
					}).then((result) => {
						if (result.value) {
							window.location.href = base_url;
						}
					});
				} else {
					swal.fire({
						position: 'center',
						type: 'warning',
						title: 'Ada kesalahan',
					});
				}
			}
		});

		return false;
	});

});
